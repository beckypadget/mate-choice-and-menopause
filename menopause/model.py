import os
import random
import settings
import copy
import numpy as np

from mymesamod import Model
from mymesamod.space import MultiGrid
from mymesamod.datacollection import DataCollector

from menopause.agents import Whale
from menopause.matriline import Matriline
from menopause.agents import ResourcePatch
from menopause.schedule import RandomActivationByBreed
from .genome import Genome


class WolfWhale(Model):

    description = "An agent-based model of Southern Resident killer whale populations for the investigation of the evolution of the menopause."
    def __init__(self, mate_choice, grandmother_effect):
        super().__init__()
        # Set parameters
        self.verbose = True
        self.resource = True
        self.height = settings.world_height
        self.width = settings.world_width
        self.initial_matrilines = settings.w_intial_number_matrilines
        self.initial_whales = settings.w_initial_population_size
        self.resource = settings.r_resource_present
        self.whales_gain_from_food = settings.w_gain_from_food
        self.whales_reproductive_cost = settings.w_reproductive_cost
        self.max_health = settings.w_max_health
        self.mate_choice = mate_choice
        self.kill_list = []
        self.whale_heaven = []
        self.all_matrilines = []
        self.grandmother_effect = grandmother_effect


        self.schedule = RandomActivationByBreed(self)
        self.grid = MultiGrid(self.height, self.width, torus=True)
        self.datacollector = DataCollector(
             {"whales": lambda m: m.schedule.get_breed_count(Whale), 
             "menopause_age": lambda m: m.get_menopause_age(),
             "female_age": lambda m: m.get_female_age(),
             "menopausal_whales": lambda m: m.get_menopausal_whales(),
             "PrR": lambda m: m.get_postreproductive_representation(),
             "female_mortality": lambda m: m.female_mortality_age(),
             "male_mortality": lambda m: m.male_mortality_age(),
             "sex_ratio": lambda m: m.get_sex_ratio(),
             "grandmother": lambda m: m.grandmother_effect})
    
        # Create whales:
        for i in range(self.initial_matrilines):
            m_x = random.randrange(self.width)
            m_y = random.randrange(self.height)
            matriline = Matriline()
            self.all_matrilines.append(matriline)
            matriline_genome = Genome.random()
            matriline_genome.is_female = 1
            matriarch = Whale(self.next_id(), (m_x, m_y), self, True,  matriline, random.gauss(settings.w_mean_age, settings.w_sd_age), mother=None, energy=random.random(), genome=matriline_genome)
            for j in range(self.initial_whales):
                w_x = m_x + random.randrange(-int(self.width * settings.w_initial_matriline_spread), int(self.width * settings.w_initial_matriline_spread)) #snazzy idea: more matriline = closer knit
                w_y = m_y + random.randrange(-int(self.height * settings.w_initial_matriline_spread), int(self.height * settings.w_initial_matriline_spread))
                energy = random.random()
                individual_genome = copy.copy(matriline_genome)
                individual_genome.mutate()
                individual_genome.assign_sex()
                age = random.gauss(settings.w_mean_age, settings.w_sd_age)
                whales = Whale(self.next_id(), (w_x, w_y), self, True,  matriline, age, mother=matriarch, energy=energy, genome=individual_genome)
                matriline.members.append(whales)
                self.grid.place_agent(whales, (w_x, w_y))
                self.schedule.add(whales)

        # Create resource patches
        if self.resource:
            for agent, x, y in self.grid.coord_iter():
                amount = random.gauss(0.5, 0.15)
                patch = ResourcePatch(self.next_id(), (x, y), self,
                                   amount)
                self.grid.place_agent(patch, (x, y))
                self.schedule.add(patch)

        self.running = True
        self.datacollector.collect(self)

    #Get stats
    def get_menopause_age(self): #gets mean menopause for females in population
        all_whales = self.schedule.agents_by_breed[Whale].values()
        female_whales = []
        for whale in all_whales:
            if whale.genome.is_female:
                female_whales.append(whale.genome.menopause_age)
        return np.mean(female_whales)

    def get_population_age(self):
        all_whales = self.schedule.agents_by_breed[Whale].values()
        ages = []
        for whale in all_whales:
            ages.append(whale.age)
        return np.mean(ages)

    def female_mortality_age(self):
        all_whales = self.schedule.agents_by_breed[Whale].values()
        mortality_ages = []
        for whale in all_whales:
            if whale.genome.is_female:
                mortality_ages.append(whale.genome.mortality_age)
        return np.mean(mortality_ages)

    def male_mortality_age(self):
        all_whales = self.schedule.agents_by_breed[Whale].values()
        mortality_ages = []
        for whale in all_whales:
            if not whale.genome.is_female:
                mortality_ages.append(whale.genome.mortality_age)
        return np.mean(mortality_ages)

    def get_max_age(self):
        all_whales = self.schedule.agents_by_breed[Whale].values()
        ages = []
        for whale in all_whales:
            ages.append(whale.age)
        return max(ages)

    def get_female_age(self):
        all_whales = self.schedule.agents_by_breed[Whale].values()
        female_whales = []
        for whale in all_whales:
            if whale.genome.is_female:
                female_whales.append(whale.age)
        return np.mean(female_whales)

    def get_population_energy(self):
        all_whales = self.schedule.agents_by_breed[Whale].values()
        energies = []
        for whale in all_whales:
            energies.append(whale.energy)
        return np.mean(energies)

    def get_menopausal_whales(self):
        menopausal_whales = 0
        for whale in self.schedule.agents_by_breed[Whale].values():
            if whale.age > whale.genome.menopause_age and whale.genome.is_female:
                menopausal_whales += 1
        return menopausal_whales

    def get_sex_ratio(self):
        female_whales = 0
        for whale in self.schedule.agents_by_breed[Whale].values():
            if whale.genome.is_female:
                female_whales += 1
        return female_whales/(len(self.schedule.agents_by_breed[Whale])+0.00001)

    def get_resource_amount(self):
        all_resource = self.schedule.agents_by_breed[ResourcePatch].values()
        energies = []
        for resource in all_resource:
            energies.append(resource.amount)
        return np.mean(energies)

    def get_postreproductive_representation(self): #gets mean PrR for population
        all_prps = []
        for dead_whale in self.whale_heaven:
            if dead_whale.genome.is_female and dead_whale.age > dead_whale.genome.menopause_age:
                postreproductive_years = max(dead_whale.age - dead_whale.genome.menopause_age, 0) #Levitis+Lackey
                indi_prp = postreproductive_years/(dead_whale.age-9.9) #check this /0 error
                all_prps.append(indi_prp)
                # print("Dead whale age:", [x.age for x in self.whale_heaven if x.genome.is_female and x.age > 10])
        if len(all_prps) == 0:
            return 0
        elif len(all_prps) == 1:
            return all_prps[-1]
        elif len(all_prps) == 2:
            return np.mean(all_prps[-2:])
        elif len(all_prps) == 3:
            return np.mean(all_prps[-3:])
        elif len(all_prps) == 4:
            return np.mean(all_prps[-4:])
        elif len(all_prps) >= 5:
            return np.mean(all_prps[-5:])

    def organise_matrilines(self):
        for matriline in self.all_matrilines:
            if len(matriline.members) == 0:
                self.all_matrilines.remove(matriline)
                # print("hello")

    def get_size_matrilines(self):
        all_sizes = []
        for matriline in self.all_matrilines:
            all_sizes.append(len(matriline.members))
            # print(len(matriline.members))
        return np.mean(all_sizes)

    def step(self):
        self.schedule.step()
        self.organise_matrilines()

        # collect data
        self.datacollector.collect(self)
        if self.verbose:
            print([self.schedule.time,
                   self.schedule.get_breed_count(Whale),
                #    self.get_max_age(),
                   self.get_menopause_age(),
                #    self.get_sex_ratio(),
                #    len(self.all_matrilines),
                #    self.female_mortality_age(),
                   self.get_postreproductive_representation()])

    def run_model(self, step_count=200):

        if self.verbose:
            print('Initial number whales: ',
                  self.schedule.get_breed_count(Whale))

        for i in range(step_count):
            self.step()

        if self.verbose:
            print('Final number whales: ',
                  self.schedule.get_breed_count(Whale))

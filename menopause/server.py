from mymesamod.visualization.ModularVisualization import ModularServer
from mymesamod.visualization.modules import CanvasGrid, ChartModule
from mymesamod.visualization.UserParam import UserSettableParameter

from menopause.agents import Whale
from menopause.agents import ResourcePatch
import settings

from menopause.model import WolfWhale


def wolf_whales_portrayal(agent):
    if agent is None:
        return

    portrayal = {}

    if type(agent) is Whale:
            portrayal["Shape"] = "menopause/resources/whale-icon.png"
            portrayal["scale"] = 0.7 * agent.age/100 + 0.3
            portrayal["Layer"] = 1


    elif type(agent) is ResourcePatch:
        maybe_colour = int((255-160) * (1-agent.amount) + 160)
        # print("Maybe colour{}".format(maybe_colour))
        resource_colour = "#9CE5{:x}".format(maybe_colour)
        portrayal["Color"] = [resource_colour, resource_colour, resource_colour]
        # print(portrayal["Color"])
        portrayal["Shape"] = "rect"
        portrayal["Filled"] = "true"
        portrayal["Layer"] = 0
        portrayal["w"] = 1
        portrayal["h"] = 1

    return portrayal

#{"Label": "Wolves", "Color": "#AA0000"}, #make this "Injured whales" and "Carers"

canvas_element = CanvasGrid(wolf_whales_portrayal, settings.world_width, settings.world_height, 500, 500)
chart_element = ChartModule([{"Label": "whales", "Color": "#666666"}, {"Label": "menopause_age", "Color": "#33cdc4"},
                            {"Label": "PrR", "Color": "#ff69b4"}, {"Label": "population_age", "Color": "#00ff00"}, {"Label": "energy", "Color": "#000000"}])
# chart_menopause = ChartModule([{"Label": "menopause_age", "Color": "#33cdc4"}])

model_params = {"mate_choice": None, "grandmother_effect": False}

server = ModularServer(WolfWhale, [canvas_element, chart_element], "Whale Menopause Evolution", model_params)
server.port = 8521

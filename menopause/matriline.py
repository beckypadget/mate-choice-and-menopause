import random
import statistics as stats

from mymesamod import Agent

from menopause.random_walk import RandomWalker

class Matriline:
    def __init__(self):
        '''
        unique_id = a code to ID each family
        members = a list of members of the matriline 
        '''
        self.members = []
        self.matriarch = None

    def GetNucleus(self):
        members_xs = []
        members_ys = []
        for whale in self.members:
            if whale.living:
                members_xs.append(whale.pos[0])
                members_ys.append(whale.pos[1])
        mean_x = stats.mean(members_xs)
        mean_y = stats.mean(members_ys)
        nucleus = mean_x, mean_y
        # print(nucleus)
        return nucleus


    def GetMatriarch(self):
        if self.matriarch is not None and self.matriarch.living:
            return self.matriarch
        member_ages = []
        for member in self.members:
            member_ages.append(member.age)
        member_ages = {x: x.age for x in self.members}
        matriarch = max(member_ages, key = member_ages.get)
        return matriarch

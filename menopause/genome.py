import random

from mymesamod import Agent

from menopause.random_walk import RandomWalker

class Genome:
    def __init__(self, menopause_age, is_female, mortality_age):
        self.menopause_age = menopause_age
        self.is_female = is_female
        self.mortality_age = mortality_age

    def mutate(self):
        key = random.choice(list(vars(self).keys())) #pick a random key (gene)
        vars(self)[key] += random.gauss(0, 5) #set the key (gene) to something else (mutant allele), 1

    def assign_sex(self):
        self.is_female = bool(random.getrandbits(1))

    @staticmethod
    def random():
        genome = Genome(random.gauss(40, 10), bool(random.getrandbits(1)), random.gauss(40, 10)) #initial range of menopause onset, set sex: false = male; true = female
        return genome

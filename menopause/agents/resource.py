import random
import settings
from mymesamod import Agent

from menopause.random_walk import RandomWalker

class ResourcePatch(Agent):
    '''
    A patch of resource that grows at a fixed rate and it is eaten by whales
    '''

    def __init__(self, unique_id, pos, model, amount):
        self.amount = amount
        self.pos = pos
        super().__init__(unique_id, model)


    def step(self):
        # self.amount += settings.r_intrinsic_growth_rate * self.amount * (1 - self.amount)
        self.amount = min(self.amount + 0.2, 1)

import random
import settings
import math
import copy
import numpy as np

from menopause.matriline import Matriline
from scipy.stats import nbinom
from scipy.optimize import minimize
from menopause.genome import Genome
from .resource import ResourcePatch
from mymesamod import Agent
from menopause.random_walk import RandomWalker

class Whale(RandomWalker):
    def __init__(self, unique_id, pos, model, moore, matriline, age, mother, energy=1, genome=None):
        super().__init__(unique_id, pos, model, moore=moore)
        self.energy = energy
        self.age = age
        self.living = True
        if genome == None:
            genome = Genome.random() ##### CHANGE THIS TO NEW GENOME 
        self.genome = genome
        self.matriline = matriline
        self.curve_param = 0.1919763 # self.get_curve_param()
        self.mother = mother

    def get_curve_param(self):
        def loss(p):
            return abs(nbinom.cdf(self.genome.menopause_age, 5, p) - settings.w_menopause_percentile)
        result = minimize(loss, 0.2, method='Nelder-Mead', tol=1e-6)
        return result.x[0]

    def step(self):
        self.CheckIfDead()
        self.model.kill_list.clear()
        # print("Age:", self.age)
        if self.living:
            self.NewMatriline()
            self.Move()
            self.Jump()
            if self.model.resource:
                self.energy = max(self.energy - settings.w_energy_expenditure, 0)
            self.Forage()
            self.TryToMate()
            self.age += 1 # each step is a year

    # These methods are for how the whale behaves with regard to the focal behaviours.
    def ChanceOfDeath(self):
        if self.genome.is_female:
            death_age = self.genome.mortality_age
            if self.age <= 10:
                chance_of_death = (0.06 * math.exp(-0.5 * (self.age)))+0.02
            elif self.age > 10 and self.age <= death_age:
                chance_of_death = 0.02
            elif self.age > death_age:
                chance_of_death = min(0.02*math.exp(0.15*(self.age-death_age)), 1)
            if self.model.grandmother_effect:
                if self.mother is not None and self.mother.mother is not None and self.mother.mother.living and self.mother.mother.matriline is self.matriline: #if grandmother is alive and in the same matriline
                    chance_of_death *= (1-settings.w_female_granny_effect)
            return chance_of_death
        else:
            death_age = (self.genome.mortality_age - 20)
            if self.age <= 10:
                chance_of_death = (0.06 * math.exp(-0.5 * (self.age)))+0.02
            elif self.age > 10 and self.age <= death_age:
                chance_of_death = 0.02
            elif self.age > death_age:
                chance_of_death = min(0.02*math.exp(0.15*(self.age-death_age)), 1)
            if self.model.grandmother_effect:
                if self.mother is not None and self.mother.mother is not None and self.mother.mother.living and self.mother.mother.matriline is self.matriline: #if grandmother is alive and in the same matriline
                    chance_of_death *= (1-settings.w_male_granny_effect)
            return chance_of_death

    def ChanceOfReproduction(self):
        x = np.arange(0, 90)
        scale = (0.2/max(nbinom.pmf(x, 5, self.curve_param))) #puts the peak in the right place
        chance_of_reproduction = scale * nbinom.pmf(np.round(self.age), 5, self.curve_param)
        if self.mother.living and self.model.grandmother_effect:
            chance_of_reproduction *= (1 - settings.w_maternal_conflict)
        return chance_of_reproduction

    # def ChanceOfReproduction(self):
    #     x = np.arange(0, 90)
    #     scale = (0.2/max(nbinom.pmf(x, 5, self.curve_param))) #puts the peak in the right place
    #     chance_of_reproduction = scale * nbinom.pmf(np.round(self.age), 5, self.curve_param)
    #     if self.mother.living and self.model.grandmother_effect:
    #         chance_of_reproduction *= (1 - settings.w_maternal_conflict)
    #     return chance_of_reproduction

    # These methods are for how the whale does basic behaviours such as move and eat.
    def Forage(self): # makes the whale take resource off the square it is on
        this_cell = self.model.grid.get_cell_list_contents([self.pos])
        resource_patch = [obj for obj in this_cell if isinstance(obj, ResourcePatch)][0]
        if resource_patch.amount > self.model.whales_gain_from_food and self.energy < 1: # and self.pos is this_cell
            self.energy = min(self.energy + self.model.whales_gain_from_food, 1)
            resource_patch.amount = max(resource_patch.amount - self.model.whales_gain_from_food, 0.01)

    def CheckIfDead(self): # checks if the whale is dead and if it is, adds it to a kill list to remove
        if self.energy <= 0 or random.random() < self.ChanceOfDeath():
            self.model.kill_list.append(self)               #add to the kill list
            self.model.grid._remove_agent(self.pos, self)   #remove it from the environment
            self.model.schedule.remove(self)                #remove it from the schedule
            self.matriline.members.remove(self)             #remove it from the matriline
            self.model.whale_heaven.append(self)            #send it to heaven
            self.living = False
            
    def TryToMate(self): # makes the whale have a calf with a probability of success
        # looks for mate, has higher chance of finding one if younger? - models male preference
        # if: living, doesn't have calf already(5 years since last), 
        def IsGoodMate(whale):
            if whale.matriline is self.matriline: #if it's unrelated
                return False
            if whale.genome.is_female: #if it's a male
                return False
            if math.sqrt((whale.pos[0]-self.pos[0])**2 + (whale.pos[1] - self.pos[1])**2) > 2: #if he is close enough
                return False
            if self.model.mate_choice is not None:
                if self.model.mate_choice is "Male":
                    if random.randrange(0,100) < self.age: #as a female's age increases she is less likely to get a mating
                        return False
                if self.model.mate_choice is "Female":
                    if random.randrange(0,100) > whale.age: #as a male's age increases he is more likely to get a mating
                        return False
            return True
                
        birth_list = [] #list of newborns so that the for loop doesn't break
        if self.living and self.genome.is_female and self.energy > settings.w_energy_threshold_for_baby: #chance of finding mate; make sure male is in other group
            for whale in self.model.schedule.agents_by_breed[Whale].values():
                if IsGoodMate(whale): #if male whale is close enough
                    if random.random() < self.ChanceOfReproduction(): # has baby maybe
                    # Create a new Whale:
                        # print("I had a baby!")
                        if self.model.resource: #If food is initiated
                            self.energy -= self.model.whales_reproductive_cost 
                        maternal_genome = self.genome
                        paternal_genome = whale.genome #sexual reproduction
                        calf_genome = copy.copy(maternal_genome)
                        calf_genome.menopause_age = np.mean([self.genome.menopause_age, whale.genome.menopause_age])
                        calf_genome.mortality_age = np.mean([self.genome.mortality_age, whale.genome.mortality_age])
                        if random.random() < settings.w_chance_mutation:
                            calf_genome.mutate()
                        calf_genome.assign_sex()
                        calf = Whale(self.model.next_id(), self.pos, self.model, self.moore, self.matriline, age=0, mother=self, energy=1, genome=calf_genome) #gives the calf the mother's genome
                        birth_list.append(calf)
                        break
            for calf in birth_list:
                self.matriline.members.append(calf)
                self.model.grid.place_agent(calf, self.pos) #calf is in the same position as mother
                self.model.schedule.add(calf)

    def Move(self): # moves a whale to a random point within a radius of their group
        # print(self.energy)
        if self.energy > settings.w_energy_threshold_to_move:
            nucleus = self.matriline.GetNucleus()
            x_vector = nucleus[0] - self.pos[0]
            y_vector = nucleus[1] - self.pos[1]
            new_x = round(self.pos[0] + x_vector * 0.25 * random.random() + 2 * random.random() - 1) # frolicking
            new_y = round(self.pos[1] + y_vector * 0.25 * random.random() + 2 * random.random() - 1)
            new_pos = new_x, new_y
            self.model.grid.move_agent(self, new_pos)
        self.living = True

    def Jump(self): # makes the matriarch move away if resources are low
        matriarch = self.matriline.GetMatriarch()
        if matriarch is self:
            nearby_thing = self.model.grid.get_cell_list_contents(self.pos)
            if type(nearby_thing) is ResourcePatch:
                if nearby_thing.amount < settings.w_gain_from_food:
                    self.random_move()

    def NewMatriline(self): # makes the matriarch move away if resources are low
        matriarch = self.matriline.GetMatriarch()
        if self.genome.is_female and len(self.matriline.members) > 4 and matriarch is not self:
            self.matriline.members.remove(self)
            new_matriline = Matriline()
            self.matriline = new_matriline
            new_matriline.members.append(self)
            new_matriline.matriarch = self
            self.model.all_matrilines.append(new_matriline)
            self.random_move()


#fix mate finding circumference

import pandas as pd

from menopause.model import WolfWhale
from mymesamod.paramsearchbatchrunner import BatchRunner

fixed_params = {"mate_choice": None}
variable_params = {"w_energy_expenditure": [0, 0.25, 0.5, 0.75], "w_reproductive_cost": [0, 0.25, 0.5, 0.75], "w_energy_threshold_to_move": [0, 0.25, 0.5, 0.75], "w_gain_from_food": [0, 0.25, 0.5, 0.75], "w_energy_threshold_for_baby": [0, 0.25, 0.5, 0.75, 1]}

# fixed_params = {"mate_choice": None, "w_energy_threshold_to_move": 0.25, "w_gain_from_food": 0.25, "w_energy_threshold_for_baby": 0.25}
# variable_params = {"w_reproductive_cost": [0, 0.25]}


def get_data_collector(model):
    return model.datacollector

batch_run = BatchRunner(WolfWhale,
                        fixed_parameters=fixed_params,
                        variable_parameters=variable_params,
                        iterations=5,
                        max_steps=1500,
                        model_reporters={"data": get_data_collector})
batch_run.run_all()

# all_dfs = {x:[] for x in variable_params["w_reproductive_cost"]} #key = mate_choice; value = list of dataframes with that mate_choice

data_dict = {"w_reproductive_cost":[], "w_energy_threshold_to_move":[], "w_gain_from_food":[], "w_energy_threshold_for_baby":[], "stability": []}# "stability_2": [], "stability_3": [], "stability_4": [], "stability_5": []}
run_data = batch_run.get_model_vars_dataframe()

for index, row in run_data.iterrows():
    for key in ["w_reproductive_cost", "w_energy_threshold_to_move", "w_gain_from_food", "w_energy_threshold_for_baby"]:
        data_dict[key].append(row[key])
    if row["data"].model_vars["whales"][-1] > 10 and row["data"].model_vars["whales"][-1] < 200:
        data_dict["stability"].append(1)
    else:
        data_dict["stability"].append(0)

df = pd.DataFrame.from_dict(data_dict)

df.to_csv("data/parameter_search.csv")

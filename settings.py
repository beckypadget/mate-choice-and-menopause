#  WORLD
world_height = 20
world_width = 20

# # WHALE
# w_grandmother_effect = True         # initialisation: whether grandmother effect is implemented
w_male_granny_effect = 0.2          # in-run: the decrease on chance of death that grandmothering has on males: bigger=better
w_female_granny_effect = 0.15       # in-run: the decrease on chance of death that grandmothering has on females: bigger=better
w_maternal_conflict = 0.2

w_initial_population_size = 3       # initialisation: number of whales in each matriline
w_intial_number_matrilines = 35     # initialisation: initial number of matrilines
w_max_health = 1                    # initialisation:
w_initial_matriline_spread = 0.1    # initialisation: how far away from each other members of a matriline initialise
w_mean_age = 20                     # initialisation: mean of initial population age distribution
w_sd_age = 10                       # initialisation: sigma of initial population age distribution
w_menopause_percentile = 0.95       # initialisation: beyond this, whale is post-reproductive

w_reproductive_cost = 0.25          # in-run: amount of energy a mother loses when giving birth
w_energy_threshold_to_move = 0      # in-run: amount of energy required for a whale to move
w_energy_threshold_for_baby = 0.7   # in-run: energy that a female must have to reproduce
w_energy_expenditure = 0.15         # in-run: energy lost per time step
w_gain_from_food = 0.5              # in-run: quantity /1 that whales take off each grid
w_chance_mutation = 0.3             # in-run: chance that there will be a mutation during reproduction

# # RESOURCE
r_resource_present = True           # initialisation: whether resources exist

r_maximum_amount = 1                # in-run: maximum amount of resources possible
# r_intrinsic_growth_rate = 0.8     # in-run: r in the logistic growth equation
